import Rhino
import System.Windows.Forms.Clipboard
import rhinoscriptsyntax as rs

def cmdHydrostatics(T):
    cmd = "-_Hydrostatics _WaterlineElevation="+str(T)+" _Symmetric=_No "
    cmd +=" _Longitude=X _Enter _Clipboard"
    
    rc = Rhino.RhinoApp.RunScript(cmd, False)
    Vol = []
    CB = []
    CF = []
    if rc:
        if System.Windows.Forms.Clipboard.ContainsText():
            text = System.Windows.Forms.Clipboard.GetText()
            
            lines = text.splitlines()
            for line in lines:
                keyVal = line.split("=")
                if keyVal[0].strip() == "Volume Displacement":
                    Vol = float(keyVal[1].replace(" ",""))
                if keyVal[0].strip() == "Center of Buoyancy":
                    CB_aux = keyVal[1].replace(" ","").split(",")
                    for cb in CB_aux:
                        CB.append(float(cb))
#                    sc = "_-Dot " + "CB w" + keyVal[1].replace(" ","")
#                    rd = Rhino.RhinoApp.RunScript(sc, False)
                elif keyVal[0].strip() == "Center of Floatation":
                    CF_aux = keyVal[1].replace(" ","").split(",")
                    for cf in CF_aux:
                        CF.append(float(cf))
#                    sc = "_-Dot " + "CF w" + keyVal[1].replace(" ","")
#                    rd = Rhino.RhinoApp.RunScript(sc, False)

    return [rc, Vol, CB, CF]


if __name__ == '__main__':
    obj = rs.AllObjects()
#    T_list = [0.0920,0.0921,0.0922,0.0923,0.0924,0.0925,0.0926,0.0927,0.0928,0.0929,0.0930]
    T_list = [9, 9.5, 10]
    for T in T_list:
        [rc, Vol, CB, CF] = cmdHydrostatics(T)
        if rc == False:
            print "Failed to calculate Hydrostatics!"
        else:
            print "*********"
            print "T = ",T
            print "Vol = ",Vol
            print "Disp = ",Vol*1.025
            print "CB = ",CB
            print "CF = ",CF

