#%%
import os

def convert(fldr = os.getcwd()):
    
    with open(fldr + '/config.wam','r') as config:
        txt_config = config.readlines()

    with open(fldr + '/config.wam','w') as config:   
        for line in txt_config:
            aux=line
            if 'ILOWHI' in line:
                aux = 'ILOWHI=0\n'
            if any([x in line for x in ['ILOWGDF','PANEL_SIZE','ITRIMWL']]):
                aux = '!' + line
            config.write(aux)

    with open(fldr + '/poten.pot','r') as poten:
        txt_pot = poten.readlines()

    with open(fldr + '/poten.pot','w') as poten:   
        for line in txt_pot:
            aux=line
            if 'ship.gdf' in line:
                aux = 'ship_low.gdf\n'
            poten.write(aux)

    print('Model Converted to Low Order')

if __name__ == '__main__':
    convert()