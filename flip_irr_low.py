#%%
import pandas as pd
import shutil
import os
import re
import sys
from adjust_low_order import readPOT
import numpy as np

def flip(fldr=os.getcwd()):
    
    padrao = "[+-]?\\d+(?:\\.\\d+)?(?:[eE][+-]?\\d+)?"

    tol = 0.001

    poten_pot = f'{fldr}/poten.pot'

    [XBODY, YBODY, ZBODY, PHI] = readPOT(poten_pot)

    print(f'ZBODY = {ZBODY:f}')

    low_order_gdf = f'{fldr}/ship_low.gdf'
    arqGDF = pd.read_csv(low_order_gdf,sep = "\s+", skiprows=4, header=None, names=('x','y','z'))
    arqGDF_copy = arqGDF

    #%% adjust z position

    pos = arqGDF.z > -ZBODY - tol

    arqGDF.z[pos] = -ZBODY

    N = len(arqGDF.z[pos])

    print(f'{N:d} vértices ajustados')

    Nlines = arqGDF.shape[0]
    pos = np.arange(0,Nlines).reshape(-1,4)

    N_panels_reverse = 0
    pos_irr = []
    for posi in pos:
        # print([x == -ZBODY for x in arqGDF.z[posi]])
        if all([x == -ZBODY for x in arqGDF.z[posi]]):
            pos_irr.append(posi[::-1])
            N_panels_reverse += 1
        else:
            pos_irr.append(posi)

    pos_irr = np.array(pos_irr).reshape(Nlines,)

    arqGDF = arqGDF.iloc[pos_irr].reset_index(drop=True)

    print(f'{N_panels_reverse:d} painéis ajustados')

    #%%

    with open(low_order_gdf) as txt:
        head = [next(txt) for x in range(4)]

    #backup of original file
    shutil.copyfile(low_order_gdf, low_order_gdf + '_irr')


    with open(low_order_gdf,'w') as txt:
        for h in head:
            txt.write(h)

    arqGDF.to_csv(low_order_gdf,mode='a',sep=' ',index=None,header=None,float_format='%.7f')

if __name__ == '__main__':
    flip()
