#%%
from operator import ge
import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist
import sys, shutil

print(sys.argv[1::])

list_panel = [eval(txt) for txt in sys.argv[1::]]

# debug
# list_panel = [eval(txt) for txt in ['366','376']]

print('Paineis: ' + str(list_panel))

list_panel = [x-1 for x in list_panel]

gdf = pd.read_csv('ship_low.gdf', header=None, skiprows=4, delim_whitespace=True)

Nlines = gdf.shape[0]

pos = np.arange(0,Nlines).reshape(-1,4)

pos_saida = pos[list_panel]
#%%
pos_saida_txt = pos[list_panel].reshape(1,-1)[0]

print('lines :' + str([x+5 for x in pos_saida_txt]))
print('')
for pos_saida_i, lp in zip(pos_saida,list_panel):
    painel_saida = gdf.iloc[pos_saida_i,:].to_numpy()

    #%%
    distances = cdist(painel_saida, painel_saida)

    d01 = distances[0][1]
    d02 = distances[0][2]
    d03 = distances[0][3]
    d12 = distances[1][2]
    d13 = distances[1][3]
    d23 = distances[2][3]

    distances = [d01, d02, d03, d12, d13, d23]

    pos_min = np.argmin(distances)
    p1 = [0,0,0,1,1,2]
    p2 = [1,2,3,2,3,3]
    pos_1 = p1[pos_min]
    pos_2 = p2[pos_min]

    painel_saida[pos_1,:] = painel_saida[pos_2,:]
    print(f'Painel {lp}')
    print(painel_saida)
    print('')
    gdf.iloc[pos_saida_i,:] = painel_saida


#%%

with open('ship_low.gdf') as txt:
    head = [next(txt) for x in range(4)]


#backup of original file
    shutil.copyfile('ship_low.gdf', 'ship_low.gdf_original')
#%%
with open('ship_low.gdf','w') as txt:
    for h in head:
        txt.write(h)

#%%
gdf.to_csv('ship_low.gdf',mode='a',sep=' ',index=None,header=None,float_format='%.7f')

# %%
