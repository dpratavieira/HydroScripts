
#%%
import re
import os
import pandas as pd

def read_output_aw(pasta):
    nome_out = 'output_aw.txt'
    arq_out_aux = open(pasta + '/' + nome_out,'r')
    arq_out = arq_out_aux.readlines()
    arq_out_aux.close()
    padrao = "[+-]?\\d+(?:\\.\\d+)?(?:[eE][+-]?\\d+)?"
        
    for x in arq_out:   
        if ('g =' in x)==True:
            [g,_] = [float(i) for i in re.findall(padrao,x)]
        if ('ULEN =' in x)==True:
            [ULEN] = [float(i) for i in re.findall(padrao,x)]
        if ('Water Depth =' in x)==True:
            aux = [float(i) for i in re.findall(padrao,x)]
            if len(aux) == 0:
                [water_depth] = [-1]
            else:
                [water_depth] = aux
            
        if ('Vols =' in x)==True:
            [volx, voly, volz, _] = [float(i) for i in re.findall(padrao,x)]
        if ('Mass =' in x)==True:
            [M] = [float(i) for i in re.findall(padrao,x)]
        if ('I =' in x)==True:
            [Ixx, Iyy, Izz, _] = [float(i) for i in re.findall(padrao,x)]
        if ('CoB =' in x)==True:
            [CoBx, CoBy, CoBz] = [float(i) for i in re.findall(padrao,x)]
        if ('CoG =' in x)==True:
            [CoGx, CoGy, CoGz] = [float(i) for i in re.findall(padrao,x)]
        if ('KG =' in x)==True:
            [KG] = [float(i) for i in re.findall(padrao,x)]
        if ('GMt =' in x)==True:
            [GMt] = [float(i) for i in re.findall(padrao,x)]
        if ('GMl =' in x)==True:
            [GMl] = [float(i) for i in re.findall(padrao,x)]

    return {'g':g, 'ULEN':ULEN, 'water_depth':water_depth, 'Vol':[volx, voly, volz], 'Mass':M, 'I':[Ixx, Iyy, Izz], 'CoB':[CoBx, CoBy, CoBz], 'CoG':[CoGx, CoGy, CoGz], 'KG':KG, 'GMt':GMt, 'GMl':GMl }




#%%
df = pd.DataFrame(columns=['Nome','L [m]', 'Lpp [m]', 'B [m]', 'D [m]', 'T [m]', 'Prof. [m]', 'M [t]', 'Ixx [t.m²]', 'Iyy [t.m²]', 'Izz [t.m²]', 'KG [m]', 'GMt [m]', 'GMl [m]', 'A. vél. Frontal [m²]', 'A. vél. Lateral [m²]', 'KGvl [m]'])
pos = 0
for root, dirs, files in os.walk("."):
    for name in files:
        if name == 'output_aw.txt':
            dados = read_output_aw(root)
            folder = root.split('/')
            folder = folder[-1]
            print(folder)
            padrao = "[+-]?\\d+(?:\\.\\d+)?(?:[eE][+-]?\\d+)?"
            props = [float(i) for i in re.findall(padrao,folder)]
            if len(props) == 4:
                [L, B, T, water_depth] = props
            if len(props) == 3:
                [L, B, T] = props

            df.loc[pos] = [folder.replace('.\\',''), L, '', B, '' , T, dados['water_depth'], dados['Mass'], dados['I'][0], dados['I'][1], dados['I'][2], dados['KG'], dados['GMt'], dados['GMl'], '', '', '']
            pos += 1
            

# %%
writer = pd.ExcelWriter("navios.xlsx", engine='xlsxwriter')
df.to_excel(writer, sheet_name='Navios', index=None)


# Get the xlsxwriter workbook and worksheet objects.
workbook  = writer.book
worksheet = writer.sheets['Navios']

# Add some cell formats.
format1 = workbook.add_format({'num_format': '0.0'})
format2 = workbook.add_format({'num_format': '0.0000E+00'})

# Set the column width and format.

worksheet.set_column(0, 0, 35, None)
worksheet.set_column(1, 6, 8, format1)

worksheet.set_column(8, 10, 12, format2)

worksheet.set_column(11, 13, 8, format1)

worksheet.set_column(14, 15, 18, None)

# Close the Pandas Excel writer and output the Excel file.
writer.close()
# %%
