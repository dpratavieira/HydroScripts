#%%
import os

def convert(fldr = os.getcwd()):
     

    with open(fldr + '/config.wam','r') as config:
        txt_config = config.readlines()

    with open(fldr + '/config.wam','w') as config:   
        for line in txt_config:
            aux=line.replace('!','')
            if 'ILOWHI' in line:
                aux = 'ILOWHI=1\n'
            config.write(aux)

    with open(fldr + '/poten.pot','r') as poten:
        txt_pot = poten.readlines()

    with open(fldr + '/poten.pot','w') as poten:   
        for line in txt_pot:
            aux=line
            if 'ship_low.gdf' in line:
                aux = 'ship.gdf\n'
            poten.write(aux)

    print('Model Converted to Hi Order')

if __name__ == '__main__':
    convert() 